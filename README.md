# README #

---

### Development

Requires [Node.JS](https://nodejs.org/en/) and uses `npm` to install dev dependencies.

* Clone repository
* Install dependencies: `npm install`
* Run dev server: `npm start`
* Build for production: `npm run build`

