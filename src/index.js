import 'normalize.css'
import './scss/main.scss'

// D-Day
const launchDate = new Date(window.d_day).getTime()
// canvas styles
const styles = {
  stroke: '#18b590',
  lineWidth: 14,
  lineCap: 'round'
}
// collection
const c = {
  ctx: {},
  values: {},
  times: {},
  degrees: {}
}
// units
const units = [
  'seconds',
  'minutes',
  'hours',
  'days'
]

// degree conversion
function deg(d) {
  return (Math.PI / 180) * d - (Math.PI / 180) * 90
}

// render canvas
function render() {
  units.forEach(function(item) {
    let degCalc = 0
    c.ctx[item].clearRect(0, 0, 200, 200)
    c.ctx[item].beginPath()
    c.ctx[item].strokeStyle = styles.stroke
    switch(item) {
      case 'seconds':
      case 'minutes':
        degCalc = deg(6 * (60 - c.times[item]))
        break;
      case 'hours':
        degCalc = deg(15 * (24 - c.times[item]))
        break;
      case 'days':
        degCalc = deg(365 - c.times[item])
        break;
    }
    c.ctx[item].arc(100, 100, 90, deg(0), degCalc)
    c.ctx[item].lineWidth = styles.lineWidth
    c.ctx[item].lineCap = styles.lineCap
    c.ctx[item].stroke()
  })
}

function init() {
  units.forEach(function(item) {
    // get 2d contexts
    c.ctx[item] = document.getElementById(item + '-canvas').getContext('2d')
    // get label values
    c.values[item] = document.getElementById(item + '-value')
  })

  setInterval(function() {
    // get todays date and time
    const now = new Date().getTime()
    // get distance from now to launchDate
    const distance = launchDate - now
    // time calculations
    c.times.days = Math.floor(distance / (1000 * 60 * 60 * 24))
    c.times.hours = Math.floor( (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60) )
    c.times.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    c.times.seconds = Math.floor((distance % (1000 * 60)) / 1000)
    // insert calculations into dom
    units.forEach(function(item) {
      c.values[item].innerText = c.times[item]
    })
    // redraw!
    render()
  }, 1000)
}

window.addEventListener('DOMContentLoaded', init)